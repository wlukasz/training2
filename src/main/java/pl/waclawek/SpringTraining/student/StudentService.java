package pl.waclawek.SpringTraining.student;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

// Class created for buisnes logic

@Service
public class StudentService {
    private final StudentDataAccessService studentDataAccessService;

    public StudentService(StudentDataAccessService studentDataAccessService) {
        this.studentDataAccessService = studentDataAccessService;
    }

    public List<Student> getAllStudents(){
        return List.of(
                new Student(UUID.randomUUID(),"James", "Bond", "jb@gmail.com", Student.Gender.MALE),
                new Student(UUID.randomUUID(),"Joanna", "Mona", "ml@hot.com", Student.Gender.FEMALE)
        );
    }

}
