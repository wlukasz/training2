package pl.waclawek.SpringTraining.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.swing.*;
import java.util.List;
import java.util.UUID;

//Class responsible for connection with data base

@Repository
public class StudentDataAccessService {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public StudentDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    List<Student> getAllStudents(){
        String sql = "" +
                "SELECT " +
                "student_id, " +
                "first_name, " +
                "last_name, " +
                "email, " +
                "gender " +
                "FROM student";                ;
        List<Student> students = jdbcTemplate.query(sql, mapStudentFromDb());
        return students;

    }

    private RowMapper<Student> mapStudentFromDb() {
        return (resultSet, i) -> {
            String uuidStr = resultSet.getString("student_id");
            UUID uuid = UUID.fromString(uuidStr);
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            String email = resultSet.getString("email");
            String genderStr = resultSet.getString("gender").toUpperCase();
            Student.Gender gender = Student.Gender.valueOf(genderStr);

            return new Student(uuid,
                    firstName,
                    lastName,
                    email,
                    gender);

        };
    }
}
